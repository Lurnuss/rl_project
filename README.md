Tech Stack
Using Vue 3

Build Setup
# Install dependencies
```
cd back_express  && npm install
```
### Open new terminal tab
```
 cd project_front && npm install
```
# Serve with hot reload at localhost:9000
```
$ cd back_express
```
```
$ npm run start
```

#### If there is an issue with server start 
``
$ pkill -f node
``
# Serve with hot reload at localhost:8080
```
$ cd project_front
```
```
$ npm run dev
```

# Launch unit tests
```
$ cd project_front
```
```
$ npm run test:unit 
```


# Launch e2e tests
```
$ cd project_front
```
```
$ npm run test:e2e 
```


## Project build and run

### Build

```
docker-compose build
```

### Run

```
docker-compose up
```
