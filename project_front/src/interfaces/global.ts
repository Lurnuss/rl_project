interface IOptionsListItems<A> {
    title: A
    value: A
}

interface IOptionSettings<T> {
    value: IOptionsListItems<T>[]
    type: string
    action: string
}
export interface IOptionsList {
    postsPerPage: IOptionSettings<number>
    sortBy: IOptionSettings<string>
}
export interface IPostList {
    posts: object[]
    nextPageUrl: string
    totalPages: number
    currentPage: number
}
export interface IPostListData {
    data: IPostList
}
export interface IPost {
    author?: string
    created_at?: string
    content?: string
    picture?: any
    title?: string
}


