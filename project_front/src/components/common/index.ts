import VSelect from "@/components/common/TheSelect.vue"
import VSearch from "@/components/common/TheSearch.vue"
import VBtn from  "@/components/common/TheButton.vue"
import VToggle from  "@/components/common/TheToggle.vue"
import VModal from "@/components/common/TheModal.vue"

export default [
    VSelect, VSearch, VBtn, VToggle, VModal
]
