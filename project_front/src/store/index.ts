import { createStore } from 'vuex'
import postsControl from './postsControl'

import createPersistentState from "vuex-persistedstate";

export default createStore({
  plugins: [createPersistentState()],
  modules: {
   postsControl,
  }
})
