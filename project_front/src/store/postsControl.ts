export default ({
    state: () => ({
        perPage: 6,
        sortBy: '',
        seenPosts: [],
    }),
    getters: {
    },
    mutations: {
        perPageUpdate: (state: { perPage: number }, payload: number) => state.perPage = payload,
        sortByUpdate: (state: { sortBy: string }, payload: string) => state.sortBy = payload,
        checkPost: (state: { seenPosts: Array<string> }, payload: string) => { if (!state.seenPosts.includes(payload)) state.seenPosts.unshift(payload) },
    },
    actions: {
    },
})
