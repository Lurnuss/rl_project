import axios from 'axios'

const API = 'http://localhost:9000/api/history/'

export default {
    getHistory(data: Array<string>) {
        return axios.post(API, data)
    },
}
