import axios from 'axios'
import {IPostListData} from "@/interfaces/global";

const API = 'http://localhost:9000/api/posts/'

export default{
    getPosts(data: {
        perPage?: number,
        page?: number,
        filtrationField?: string,
        sortBy?: string,
    }): Promise<IPostListData> {
        return axios.get(`${API}`, {
            params: {
                perPage: data.perPage,
                page: data.page,
                filtrationField: data.filtrationField,
                sortBy: data.sortBy,
            }
        })
    },
    loadMorePosts(api: string):Promise<IPostListData> {
        return axios.get(api)
    },
    createPost(data: object): Promise<object> {
        return axios.post(API, data)
    },
    getPostById(id: string): Promise<object> {
        return axios.get(`${API}${id}`)
    },
    updatePost(data: object): Promise<object> {
        return axios.put(API, data)
    },
    deletePost(id: string): Promise<object>{
        return axios.delete(`${API}${id}`)
    },
}
