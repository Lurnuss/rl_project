import { createApp } from 'vue'
import App from './App.vue'
import components from '@/components/common'
import router from './router'
import store from './store'
import i18n from './i18n'

const app = createApp(App)

components.forEach(component => {
    app.component(component.name, component)
})
app.use(store).use(i18n).use(router).mount('#app')
