import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router'
import HomeView from '../views/HomeView.vue'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'home',
    component: HomeView,
  },
  {
    path: '/post/:id',
    name: 'SinglePost',
    component: () => import(`../views/singlePost.vue`)
  },
  {
    path: '/history/',
    name: 'SeenPosts',
    component: () => import(`../views/SeenPosts.vue`)
  }
]

const router = createRouter({
  history: createWebHistory(),
  routes
})

export default router
