import { shallowMount } from '@vue/test-utils'
import SinglePost from "@/views/singlePost.vue"
import VModal from "@/components/common/TheModal.vue"
import { createStore } from 'vuex'
import postsControl from "@/store/postsControl";
import { useRouter, useRoute } from 'vue-router'

const createNewStore = () => createStore({ modules: { postsControl: postsControl}})

function factory() {
    const store: any = createNewStore()
// @ts-ignore
    useRoute.mockImplementationOnce(() => ({
        params: {
            id: '13213213'
        }
    }))
    const push = jest.fn()
    // @ts-ignore
    useRouter.mockImplementationOnce(() => ({
        push
    }))
    return shallowMount(SinglePost, {
        global: {
            plugins: [store],
            stubs: ["router-link", "router-view"],
            mocks: {
                VModal: VModal,
            },
        },
    })
}

const mockGet = jest.fn()

jest.mock('axios', () => ({
    get: () => mockGet(),
}))
jest.mock('vue-router', () => ({
    useRoute: jest.fn(),
    useRouter: jest.fn(() => ({
        push: () => {}
    }))
}))


describe('Single post page', () => {
    it('renders correctly', () => {
        const wrapper = factory()
        expect(wrapper.element).toMatchSnapshot()
    })
    it('makes API call', async () => {
        const wrapper = factory()
        expect(mockGet).toHaveBeenCalled()
    })
})
