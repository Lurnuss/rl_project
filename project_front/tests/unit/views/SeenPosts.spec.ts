import { shallowMount } from '@vue/test-utils'
import SeenPosts from "@/views/SeenPosts.vue"
import { createStore } from 'vuex'
import postsControl from "@/store/postsControl";

const createNewStore = () => createStore({ modules: { postsControl: postsControl}})

function factory() {
    const store: any = createNewStore()

    return shallowMount(SeenPosts, {
        global: {
            plugins: [store],
        }
    })
}

const mockGet = jest.fn()

jest.mock('axios', () => ({
    post: () => mockGet(),
}))

describe('Posts history ', () => {
    it('renders correctly', () => {
        const wrapper = factory()
        expect(wrapper.element).toMatchSnapshot()
    })
    it('makes API call', async () => {
        const wrapper = factory()
        expect(mockGet).toHaveBeenCalled()
    })
})
