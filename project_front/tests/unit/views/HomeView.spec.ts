import { shallowMount } from '@vue/test-utils'
import HomeView from "@/views/HomeView.vue"

function factory() {
    return shallowMount(HomeView, {
    })
}

describe('Home page', () => {
    it('renders correctly', () => {
        const wrapper = factory()
        expect(wrapper.element).toMatchSnapshot()
    })
})
