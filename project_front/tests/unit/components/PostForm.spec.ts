import { shallowMount } from '@vue/test-utils'
import PostForm from "@/components/PostForm.vue"

function factory(id?: string) {
    const post = {
        title: 'test',
        content: 'test second',
        _id: id,
        created_at: '11-09-2001 09:30',
        picture: 'somePic',
    }
    return shallowMount(PostForm, {
        props: { post },
        global: {
            mocks: {
                $t: (msg: string) => msg
            },
        },
    })
}

describe('Post creation form', () => {
    it('press save button triggers an update emmit with object inside', async () => {
        const wrapper = factory('someId123')
        await wrapper.find('.save_button').trigger('click')
        expect(wrapper.emitted()['update'].flat()[0]).not.toBeUndefined()
    })
    it('press save button triggers an create emmit with object inside', async () => {
        const wrapper = factory()
        await wrapper.find('.save_button').trigger('click')
        expect(wrapper.emitted()['create'].flat()[0]).not.toBeUndefined()
    })
    it('renders correctly', () => {
        const wrapper = factory()
        expect(wrapper.element).toMatchSnapshot()
    })
})
