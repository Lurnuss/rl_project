import { shallowMount } from '@vue/test-utils'
import Post from "@/components/ThePost.vue"
import { createStore } from 'vuex'
import postsControl from "@/store/postsControl";

const createNewStore = () => createStore({ modules: { postsControl: postsControl}})

const post = {
    title: 'test',
    content: 'test second',
    _id: '123321',
    created_at: '11-09-2001 09:30'
}
const EditMode = true

function factory() {
    const store: any = createNewStore()

    return shallowMount(Post, {
        props: { post, EditMode },
        global: {
            plugins: [store],
            mocks: {
                $t: (msg: string) => msg
            },
        },
    })
}

const mockDelete = jest.fn()

jest.mock('axios', () => ({
    delete: () => mockDelete(),
}))

describe('Post List', () => {
    it('renders correctly', () => {
        const wrapper = factory()
        expect(wrapper.element).toMatchSnapshot()
    })
    it('press edit button triggers an edit emmit without object inside', async () => {
        const wrapper = factory()
        await wrapper.find('.editPost_btn').trigger('click')
        expect(wrapper.emitted()['editPost'].flat()[0]).toBeUndefined()
    })
    it('press delete button triggers delete request', async () => {
        const wrapper = factory()
        await wrapper.find('.deletePost_btn').trigger('click')
        expect(mockDelete).toHaveBeenCalled()
    })
})
