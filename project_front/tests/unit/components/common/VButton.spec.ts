import { mount } from '@vue/test-utils'
import VButton from "@/components/common/TheButton.vue"

function factory() {
    const msg = 'text inside button'
    return mount(VButton, {
        slots: {
            default: msg
        }
    })
}

describe('Custom Button', () => {
    it('renders message within slot', () => {
        const msg = 'text inside button'
        const wrapper = factory()
        expect(wrapper.html()).toContain(`<span>${msg}</span></button>`)
    })
    it('renders correctly', () => {
        const wrapper = factory()
        expect(wrapper.element).toMatchSnapshot()
    })
})
