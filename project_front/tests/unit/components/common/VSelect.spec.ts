import { mount } from '@vue/test-utils'
import VSelect from "@/components/common/TheSelect.vue"
import { createStore } from 'vuex'
import postsControl from "@/store/postsControl";

const createNewStore = () => createStore({ modules: { postsControl: postsControl}})
const option: any = {
    value: [
        {
            title: 6,
            value: 6
        },
        {
            title: 12,
            value: 12
        }
    ],
    type: 'perPage',
    action: 'perPageUpdate'
}

function factory() {
    const store: any = createNewStore()
    return mount(VSelect, {
        global: {
            plugins: [store],
            mocks: {
                $t: (msg: string) => msg
            },
        },
        props: { option }
    })
}

describe('Custom Select', () => {
    it('component\'s emmit check', async () => {
        const value = 12
        const wrapper = factory()
        await wrapper.find('select').setValue(value)
        expect(wrapper.emitted()['change'].flat()[0]).toBe(value)
    })
    it('renders correctly', () => {
        const wrapper = factory()
        expect(wrapper.element).toMatchSnapshot()
    })
})
