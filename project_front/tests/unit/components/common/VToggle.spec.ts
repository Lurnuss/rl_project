import { mount } from '@vue/test-utils'
import VToggle from "@/components/common/TheToggle.vue"

describe('Custom Toggle', () => {
    it('component\'s emmit check', async () => {
        const wrapper = mount(VToggle)
        await wrapper.find('input').trigger('click')
        expect(wrapper.emitted()['toggle'].flat()[0]).toBeUndefined()
    })
    it('renders correctly', () => {
        const wrapper = mount(VToggle)
        expect(wrapper.element).toMatchSnapshot()
    })
})
