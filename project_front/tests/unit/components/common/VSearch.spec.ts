import { mount } from '@vue/test-utils'
import VSearch from "@/components/common/TheSearch.vue"

function factory() {
    const placeHolderText = 'place holder'
    return mount(VSearch, {
        props: { placeHolderText }
    })
}

describe('Custom Search', () => {
    it('placeholder renders when prop placeHolderText was transferred', () => {
        const placeHolderText = 'place holder'
        const wrapper = factory()
        expect(wrapper.html()).toContain(`placeholder="${placeHolderText}"`)
    })
    it('changing an input value is triggering an emmit with new value inside', async () => {
        const inputText = 'Test string'
        const wrapper = factory()
        await wrapper.find('input').setValue(inputText)
        expect(wrapper.emitted()['input'].flat()[0]).toBe(inputText)
    })
    it('renders correctly', () => {
        const wrapper = factory()
        expect(wrapper.element).toMatchSnapshot()
    })
})
