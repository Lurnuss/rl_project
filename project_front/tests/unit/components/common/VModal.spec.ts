import {mount, shallowMount} from '@vue/test-utils'
import VModal from "@/components/common/TheModal.vue"
import PostList from "@/components/PostList.vue";


function factory(show: boolean) {

    return mount(VModal, {
        props: { show }
    })
}

describe('Custom Modal', () => {
    it('renders when prop show is true', () => {
        const wrapper = factory(true)
        expect(wrapper.html()).toContain('div')
    })
    it('renders when prop show is false', () => {
        const wrapper = factory(false)
        expect(wrapper.html()).not.toContain('div')
    })
    it('renders correctly', () => {
        const wrapper = factory(true)
        expect(wrapper.element).toMatchSnapshot()
    })
    it('emmit close event on clicking outside part of the modal', async () => {
        const wrapper = factory(true)
        await wrapper.find('.dialog').trigger('click')
        expect(wrapper.emitted()['toggle'].flat()[0]).toBeUndefined()
    })
})
