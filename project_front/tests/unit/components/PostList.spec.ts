import { shallowMount } from '@vue/test-utils'
import PostList from "@/components/PostList.vue"
import { createStore } from 'vuex'
import postsControl from "@/store/postsControl";

const createNewStore = () => createStore({ modules: { postsControl: postsControl}})

function factory() {
    const store: any = createNewStore()
    return shallowMount(PostList, {
        global: {
            plugins: [store],
            mocks: {
                $t: (msg: string) => msg,

            },
        },
    })
}
const mockGet = jest.fn()

jest.mock('axios', () => ({
    get: () => mockGet(),
}))
describe('Post List', () => {
    it('makes API call when load', async () => {
        const wrapper = factory()
        expect(mockGet).toHaveBeenCalled()
    })
    it('renders correctly', () => {
        const wrapper = factory()
        expect(wrapper.element).toMatchSnapshot()
    })
    it('calls filter function on input', async () => {
        const wrapper = factory()
        const spyOnFilter = jest.spyOn(wrapper.vm, 'filterPosts')
        await wrapper.find('[data-testId="search-posts"]').trigger('input')
        expect(spyOnFilter).toHaveBeenCalled()
    })
    it('calls get-posts function with an get API endpoint call', async () => {
        const wrapper = factory()
        const spyOnPostsPerPage = jest.spyOn(wrapper.vm, 'getPosts')
        await wrapper.find('[data-testId="posts-per-page"]').trigger('change')
        expect(spyOnPostsPerPage).toHaveBeenCalled()
    })
    it('calls get-posts function with an get API endpoint call', async () => {
        const wrapper = factory()
        const spyOnSort = jest.spyOn(wrapper.vm, 'getPosts')
        await wrapper.find('[data-testId="sort-posts"]').trigger('change')
        expect(spyOnSort).toHaveBeenCalled()
    })

    it('opens modal on editPost method call',  () => {
        const wrapper = factory()
        wrapper.vm.editPost()
        expect(wrapper.vm.showModal).toBeTruthy()
    })
    it('opens modal on createPost method call',  () => {
        const wrapper = factory()
        wrapper.vm.createPost()
        expect(wrapper.vm.showModal).toBeTruthy()
        expect(wrapper.vm.currentPost).toStrictEqual({author: 'Bohdan'})
    })
})
