// https://docs.cypress.io/api/table-of-contents

describe('Single post page test', () => {

	it('Click on post opens the same post', () => {
		cy.visit('/')
		let previousId = ''
		let currentId = ''
			cy.get('[data-testId="post"]')
			.first()
			.invoke('attr', 'data-testPostId')
				.then(id => previousId = id)

		cy.get('[data-testId="post"]')
			.first()
			.click()

		cy.get('[data-testId="post"]')
			.invoke('attr', 'data-testPostId')
			.then(id => currentId = id)
		expect(currentId).to.equal(previousId)
	})

	it('Edit modal works correctly', () => {
		cy.visit('/')
		cy.intercept('PUT', 'http://localhost:9000/api/posts', { fixture: 'post.json' }).as('updatePost')
		cy.get('[data-testId="post"]')
			.first()
			.click()
		cy.get('.editPost_btn')
			.click()

		cy.get('.dialog')
			.should('be.visible')

		cy.get('.dialog')
			.click('bottomRight')
			.should('not.exist')

		cy.get('.editPost_btn')
			.click()

		let postTitle = ''
		let modalTitleInput = ''
		cy.get('.post__item-title')
			.invoke('text')
			.then(text => postTitle = text)
		cy.get('[data-testId="modal-title"]')
			.invoke('text')
			.then(text => modalTitleInput = text)
		expect(postTitle).to.equal(modalTitleInput)

		cy.get('.save_button')
			.click()

		cy.wait('@updatePost')
		cy.get('@updatePost')
			.then(xhr => {
				expect(xhr.response.statusCode).to.equal(200)
			})

		cy.get('.dialog')
			.should('not.exist')
	})

	it('Delete modal works correctly', () => {
		cy.visit('/')
		cy.intercept('DELETE', 'http://localhost:9000/api/posts/**', { fixture: 'post.json' }).as('deletePost')
		cy.get('[data-testId="post"]')
			.first()
			.click()
		cy.wait(500)

		cy.get('.deletePost_btn')
			.click()

		cy.wait('@deletePost')
		cy.get('@deletePost')
			.then(xhr => {
				expect(xhr.response.statusCode).to.equal(200)
			})

		cy.url().should('not.include', '/post/')
	})
})
