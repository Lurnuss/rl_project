// https://docs.cypress.io/api/table-of-contents
import uk from'../../../src/locales/uk.json'

describe('Home page test', () => {
  beforeEach(() => {
    cy.visit('/')
  })
  it('control panel translation check', () => {
    cy.get('.lang-select')
        .select('uk')
    cy.get('[data-testId="search-posts"]')
        .invoke('attr', 'placeholder').should('contain', `${uk.search}...`)
    cy.get('[data-testId="sort-posts"]')
        .select('title')
        .should('contain', uk.title)
    cy.get('[data-testId="create-post"]')
        .should('contain', uk.create_post)
  })

  it('filter posts on input', () => {
    const someText = 'textSome'
    cy.get('[data-testId="search-posts"]')
        .type(someText).wait(1000)

    cy.get('[data-testId="post"]').each(item => expect(Cypress.$(item).text()).to.contain(someText))
  })

  it('Turn on edit mode for posts', () => {
    cy.get('[data-testId="edit-mode"]')
        .click()
    cy.get('.btn_container')
        .should('be.visible')
  })

  it('Tests Create button and modal window workability', () => {
    cy.get('[data-testId="create-post"]')
        .click()

    cy.get('[data-testId="modal"]')
        .should('be.visible')

    cy.get('.dialog')
        .click('bottomRight')
        .should('not.exist')
  })

  it('Tests workability of the post creation button', () => {
    cy.intercept('POST', 'http://localhost:9000/api/posts/', { fixture: 'post.json' }).as('createdPost')

    cy.get('[data-testId="create-post"]')
        .click()

    cy.get('[data-testId="modal"]')
        .should('be.visible')

    cy.get('.save_button')
        .click()

    cy.get('[data-testId="modal"]')
        .should('be.visible')

    cy.get('[data-testId="modal-title"]')
        .type('textSome')

    cy.get('[data-testId="modal-content"]')
        .type('textSomeContent')

    cy.get('[data-testId="modal-file"]')
        .selectFile('tests/e2e/fixtures/files/bg_4.jpg', { force: true })

    cy.get('.save_button')
        .click()

    cy.wait('@createdPost')
    cy.get('@createdPost').then(xhr => {
      const bodyString = Cypress.Blob.arrayBufferToBinaryString(xhr.request.body);
      const body = bodyString.substr(0, +bodyString.indexOf('textSomeContent') + 'textSomeContent'.length)
      expect(body).to.contain('Bohdan').and.to.contain('textSome').and.to.contain('textSomeContent')
    })

    cy.get('.dialog')
        .should('not.exist')
  })

})
