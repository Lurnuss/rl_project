// https://docs.cypress.io/api/table-of-contents
import uk from'../../../src/locales/uk.json'

describe('History page test', () => {
	beforeEach(() => {
		cy.visit('/')
	})
	it('Tests workability of the history page, we check if the post exist on history page, after click on it. We create post, then delete', () => {
		const postTitle = 'Here just some unique title, 2931920401294124'
		cy.intercept('POST', 'http://localhost:9000/api/posts/').as('createdPost')
		cy.intercept('DELETE', 'http://localhost:9000/api/posts/**').as('deletedPost')


			cy.get('[data-testId="create-post"]')
				.click()

			cy.get('[data-testId="modal"]')
				.should('be.visible')


			cy.get('[data-testId="modal-title"]')
				.type(postTitle)

			cy.get('[data-testId="modal-content"]')
				.type('textSomeContent')

			cy.get('[data-testId="modal-file"]')
				.selectFile('tests/e2e/fixtures/files/bg_4.jpg', {force: true})

			cy.get('.save_button')
				.click()

			cy.wait('@createdPost')
			cy.get('@createdPost').then(xhr => {
				const createdPostId = xhr.response.body._id

				cy.get('.dialog')
					.should('not.exist')

				cy.get('[data-testId="history-page"]')
					.click()

				cy.get(`[data-testPostId="${createdPostId}"]`)
					.should('not.exist')

				cy.get('[data-testId="home-page"]')
					.click()

				cy.get('[data-testId="search-posts"]')
					.type(postTitle).wait(1000)

				cy.get(`[data-testPostId="${createdPostId}"]`)
					.should('be.visible')
					.click()

				cy.get('[data-testId="history-page"]')
					.click()

				cy.get(`[data-testPostId="${createdPostId}"]`)
					.should('be.visible')
					.click()


				cy.get('.deletePost_btn')
					.click()

				cy.wait('@deletedPost')
				cy.get('@deletedPost')
					.then(xhr => {
						expect(xhr.response.statusCode).to.equal(200)
					})
			})

	})

	it('tests history page for 3 posts', () => {
		const seenPostsList = []
		const allSeenPostsIds=  []
		const arr = [1,2,3]

		arr.forEach(item => {
			cy.get('[data-testId="post"]')
				.eq(item)
				.invoke('attr', 'data-testPostId')
				.then(id => seenPostsList.push(id))

			cy.get('[data-testId="post"]')
				.eq(item)
				.click()

			cy.get('[data-testId="home-page"]')
				.click()
		})

		cy.get('[data-testId="history-page"]')
			.click()

		cy.get('[data-testId="post"]')
			.each($el => {
				cy.wrap($el).invoke('attr', 'data-testPostId')
					.then(id => allSeenPostsIds.push(id))
			})
		expect(allSeenPostsIds).to.deep.eq(seenPostsList)
	})
})
