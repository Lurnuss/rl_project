import Post from "./Post.js";
import fileService from "./fileService.js";

class PostService {
	async  create (post, picture) {
		const fileName = fileService.saveFile(picture)
		const createdPost = await Post.create({...post, picture: fileName})
		return createdPost
	}
	async getAll() {
			const posts = await Post.find()
			return posts
	}
	async getOne(id) {
		if (!id) throw new Error("Id was not entered")
		const post = await Post.findById(id)
		if (!post) throw new Error("Post with this id was not found")
		return post
	}
	async update(post) {
			if (!post._id) throw new Error('Invalid Id')
			const updatedPost = await Post.findByIdAndUpdate(post._id, post, { new: true })
			return updatedPost
	}
	async delete(id) {
			if (!id) throw new Error ("No id was entered")
			const post = await Post.findByIdAndDelete(id)
			return post
	}
}


export default new PostService()
