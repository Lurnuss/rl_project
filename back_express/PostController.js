import Post from "./Post.js";
import PostService from "./PostService.js";

class PostController {
	async  create (req, res) {
		try {
			const post = await PostService.create(req.body, req.files.picture)
			res.json(post)
		} catch (e) {
			res.status(500).json(e)
		}
	}
	async getAll(req, res) {
		try {
			let allPosts = await PostService.getAll()
			let posts = allPosts.reverse()
			const data = req.query
			data.perPage = +data.perPage ?? 6
			data.page  = +data.page ?? 1
			const queryParams = data ? '?' + Object.entries(data)
				.map(item => item[0] === 'page' ? `${item[0]}=${+item[1] + 1}` : `${item[0]}=${item[1]}`)
				.join('&') : ''

			if (data?.filtrationField?.trim()) posts = posts.filter(item => item.title.includes(data.filtrationField.trim()))
			if (data?.sortBy) posts.sort((a, b) => a[data.sortBy] > b[data.sortBy] ? 1 : -1)

			let subArray = [];
			for (let i = 0; i <Math.ceil(posts.length/data.perPage); i++){
				subArray[i] = posts.slice((i*data.perPage), (i*data.perPage) + data.perPage);
			}
			const nextPageUrl = +data.page < subArray.length ? `http://localhost:9000/api/posts/${queryParams}` : null
			const response = {
				posts: subArray[+data.page - 1] ?? [],
				currentPage: +data.page,
				totalPages: subArray.length,
				nextPageUrl: nextPageUrl,
			}
			return res.json(response)
		} catch (e) {
			res.status(500).json(e)
		}
	}
	async getOne(req, res) {
		try {
			const post = await PostService.getOne(req.params.id)
			return res.json(post)
		} catch (e) {
			res.status(500).json(e)
		}
	}
	async update(req, res) {
		try {
			const updatedPost = await PostService.update(req.body)
			return res.json(updatedPost)
		} catch (e) {
			res.status(500).json(e)
		}
	}
	async delete(req, res) {
		try {
			const post = await PostService.delete(req.params.id)
			return res.json(post)
		} catch (e) {
			res.status(500).json(e)
		}
	}

	async getHistory(req, res) {
		try {
			const history = req.body
			const posts = await PostService.getAll()
			const seenPosts = posts.filter(item => history.includes(item._id.toString())).reverse()
			res.json(seenPosts)
		} catch (e) {
			res.status(500).json(e)
		}
	}
}

export default new PostController()
