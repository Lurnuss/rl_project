import cors from 'cors'

const corsOptions = {
	origin: ['http://localhost:8080', 'http://localhost:8081'],
	optionsSuccessStatus: 200,
}

export default cors(corsOptions)
